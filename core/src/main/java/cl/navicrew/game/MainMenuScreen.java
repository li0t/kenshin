package cl.navicrew.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;


/**
 * Created by liot on 10/18/14.
 */
public class MainMenuScreen implements Screen {
    private final KenshinAdventure game;
    private OrthographicCamera camera;

    public MainMenuScreen(KenshinAdventure game){
        this.game = game;
        game.reset();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.spriteBatch.setProjectionMatrix(camera.combined);

        game.spriteBatch.begin();
        game.font.setScale(2);
        game.font.draw(game.spriteBatch, "Super Jumper2D", 100, 500);
        game.font.draw(game.spriteBatch, "Toca la pantalla para jugar", 100, 450);
        game.font.draw(game.spriteBatch, "Pulsa ESCAPE para salir", 100, 400);
        game.spriteBatch.end();

        /*
		 * Si el usuario toca la pantalla se inicia la partida
		 */
        if (Gdx.input.isTouched()) {
            game.setScreen(new GameScreen(game));
            dispose();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            dispose();
            System.exit(0);
        }


    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
