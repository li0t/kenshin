package cl.navicrew.game;

import cl.navicrew.game.util.Directions;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;
import java.util.Map;

public class KenshinAdventure extends Game {

    public SpriteBatch spriteBatch;
    private OrthographicCamera camera;
    public BitmapFont font;
    public Texture itemCoin;
    public Texture itemLife;
    public int coins, lives, level;
    private TextureAtlas spriteSheet;
    private Array<Sprite> atlasRegions;
    private TextureRegion kenCurrentFrame, enemyCurrentFrame;
    private final float ATTACK_RATE = 0.2f;
    public Music music;
    public Sound coinSound, jumpSound, downSound, gameOverSound, levelClearSound, levelSound;
    private float stateTime, timeBetweenAttacks;
    private Vector2 kenPos, enemyPos;
    private Map<Directions, Animation> kenIdle, kenWalk, kenJump, kenDash, kenGuard, kenBasicAttack, kenBasicAttackMove,
            kenBasicAttackUp, kenBasicAttackDown, enemyIdle, enemyWalk, enemyBasicAttack, enemyBasicAttackMove, enemyJump;
    private Directions kenDirection, enemyDirection;

    @Override
    public void create() {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
        spriteBatch = new SpriteBatch();
        kenPos = new Vector2(350, 150);
        kenIdle = new HashMap<Directions, Animation>();
        kenWalk = new HashMap<Directions, Animation>();
        kenJump = new HashMap<Directions, Animation>();
        kenDash = new HashMap<Directions, Animation>();
        kenGuard = new HashMap<Directions, Animation>();
        kenBasicAttack = new HashMap<Directions, Animation>();
        kenBasicAttackMove = new HashMap<Directions, Animation>();
        kenBasicAttackUp = new HashMap<Directions, Animation>();
        kenBasicAttackDown = new HashMap<Directions, Animation>();
        kenDirection = Directions.RIGHT;
        music = Gdx.audio.newMusic(Gdx.files.internal("music/Noriyuki_asakura_battousai_hutatabi_original_mix_TaringaMp3.Com.mp3"));
        loadResources();
        music.setLooping(true);
        music.play();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {
      super.render();
    }


    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        spriteBatch.dispose();
        itemCoin.dispose();
        itemLife.dispose();
        font.dispose();
        spriteSheet.dispose();
        music.dispose();
        coinSound.dispose();
        jumpSound.dispose();
        downSound.dispose();
        gameOverSound.dispose();
        levelClearSound.dispose();
        levelSound.dispose();
    }

    private void loadResources() {
        spriteSheet = new TextureAtlas(Gdx.files.internal("kenshin.txt"));

        //kenidleleft
        atlasRegions = spriteSheet.createSprites("kenshinidleleft");
        kenIdle.put(Directions.LEFT, new Animation(0.35f, atlasRegions, Animation.PlayMode.LOOP));

        //kenidleright
        atlasRegions = spriteSheet.createSprites("kenshinidleright");
        kenIdle.put(Directions.RIGHT, new Animation(0.35f, atlasRegions, Animation.PlayMode.LOOP));

        //kenwalkingleft
        atlasRegions = spriteSheet.createSprites("kenshinrunleft");
        kenWalk.put(Directions.LEFT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //kenwalkingright
        atlasRegions = spriteSheet.createSprites("kenshinrunright");
        kenWalk.put(Directions.RIGHT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //kendashleft
        atlasRegions = spriteSheet.createSprites("kenshindashleft");
        kenDash.put(Directions.LEFT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //kendashright
        atlasRegions = spriteSheet.createSprites("kenshindashright");
        kenDash.put(Directions.RIGHT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //kenjumpleft
        atlasRegions = spriteSheet.createSprites("kenshinjumpleft");
        kenJump.put(Directions.LEFT, new Animation(0.19f, atlasRegions, Animation.PlayMode.LOOP));

        //kenjumpright
        atlasRegions = spriteSheet.createSprites("kenshinjumpright");
        kenJump.put(Directions.RIGHT, new Animation(0.19f, atlasRegions, Animation.PlayMode.LOOP));

        //kenguardleft
        atlasRegions = spriteSheet.createSprites("kenshinguardleft");
        kenGuard.put(Directions.LEFT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //kenguardright
        atlasRegions = spriteSheet.createSprites("kenshinguardright");
        kenGuard.put(Directions.RIGHT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //kenbasicattackleft
        atlasRegions = spriteSheet.createSprites("kenshinbasicattackleft");
        kenBasicAttack.put(Directions.LEFT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        ////kenbasicattackright
        atlasRegions = spriteSheet.createSprites("kenshinbasicattackright");
        kenBasicAttack.put(Directions.RIGHT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //kenbasicattackmovingleft
        atlasRegions = spriteSheet.createSprites("kenshinbasicattackleftmove");
        kenBasicAttackMove.put(Directions.LEFT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        ////kenbasicattackmovingright
        atlasRegions = spriteSheet.createSprites("kenshinbasicattackrightmove");
        kenBasicAttackMove.put(Directions.RIGHT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //kenbasicattackupleft
        atlasRegions = spriteSheet.createSprites("kenshinbasicattackupleft");
        kenBasicAttackUp.put(Directions.LEFT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        ////kenbasicattackupright
        atlasRegions = spriteSheet.createSprites("kenshinbasicattackupright");
        kenBasicAttackUp.put(Directions.RIGHT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        //kenbasicattackdownleft
        atlasRegions = spriteSheet.createSprites("kenshinbasicattackdownleft");
        kenBasicAttackDown.put(Directions.LEFT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

        ////kenbasicattackdownright
        atlasRegions = spriteSheet.createSprites("kenshinbasicattackdownright");
        kenBasicAttackDown.put(Directions.RIGHT, new Animation(0.1f, atlasRegions, Animation.PlayMode.LOOP));

    }

    public void reset(){

    }




}
