package cl.navicrew.game;


import cl.navicrew.game.characters.Player;
import cl.navicrew.game.util.Directions;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;


/**
 * Created by liot on 10/18/14.
 */
public class GameScreen implements Screen {
    private final KenshinAdventure game;
    private TiledMap map;
    private OrthogonalTiledMapRenderer mapRenderer;
    private Player player;
    private OrthographicCamera camera;
    private boolean starting;

    public GameScreen(KenshinAdventure game) {
        this.game = game;
        starting = true;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        handleInput();
        player.update(delta);

        //Fixes the camera to follow the player at the center of the screen
        camera.position.set(player.position.x + 18 / 2, 125, 0);
        camera.zoom = 1 / 2f;
        camera.update();
        mapRenderer.setView(camera);
        mapRenderer.render();

        mapRenderer.getSpriteBatch().begin();

        player.render(mapRenderer.getSpriteBatch());

        game.font.setScale(0.8f);
        mapRenderer.getSpriteBatch().draw(game.itemCoin, camera.position.x - 60, camera.position.y - 135 - 12);
        game.font.draw(mapRenderer.getSpriteBatch(), " X " + game.coins, camera.position.x - 50, camera.position.y - 135);
        mapRenderer.getSpriteBatch().draw(game.itemLife, camera.position.x + 40, camera.position.y - 135 - 12);
        game.font.draw(mapRenderer.getSpriteBatch(), " X " + game.lives, camera.position.x + 50, camera.position.y - 135);
        mapRenderer.getSpriteBatch().end();

    }

    @Override
    public void resize(int width, int height) {

    }

    /**
     * Called when this screen becomes the current screen for a {@link com.badlogic.gdx.Game}.
     */
    @Override
    public void show() {
        if (starting) {
            //Creates map and renderer
            map = new TmxMapLoader().load("jumperAssets/levels/level1.tmx");
            mapRenderer = new OrthogonalTiledMapRenderer(map);

            camera = new OrthographicCamera();
            camera.setToOrtho(false, 30, 20);
            camera.update();

            // Obtains the reference of the terrain layer
            TiledMapTileLayer collisionLayer = (TiledMapTileLayer) map.getLayers().get(0);
            // Creates player and set's it on the screen
            player = new Player(collisionLayer, game);
            // Player starting position
            player.position.set(0 * map.getProperties().get("tilewidth", Integer.class), 2 * map.getProperties().get("tileheight", Integer.class) + 32);

            game.music.play();

            starting = false;
        }
    }

    private void handleInput() {

        // Se pulsa la teclad derecha
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            player.isRunning = true;
            player.velocity.x = Player.WALKING_SPEED;
            player.state = Player.State.WALKING_RIGHT;

            if ((!player.isJumping))
                player.isWalking = true;
        }
        // Se pulsa la tecla izquierda
        else if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            player.isRunning = true;
            player.velocity.x = -Player.WALKING_SPEED;
            player.state = Player.State.WALKING_LEFT;

            if ((!player.isJumping))
                player.isWalking = true;
        }
        // No se pulsa ninguna tecla
        else {

            if (player.isRunning)
                if (player.direction == Directions.LEFT)
                    player.state = Player.State.IDLE_LEFT;
                else
                    player.state = Player.State.IDLE_RIGHT;

            player.isRunning = false;
            player.isWalking = false;
            player.velocity.x = 0;
        }


		/*
         * El usuario pulsa la tecla de Pausa (P)
		 * El juego se detiene almacenando el estado de la partida (this)
		 */
        if (Gdx.input.isKeyPressed(Input.Keys.P)) {
            game.setScreen(new PauseScreen(game, PauseScreen.State.PAUSE, this));
        }

        // Controla los límites (por debajo) de la pantalla, cuando cae el personaje
        if (player.position.y < 0) {
            game.lives--;
            if (game.lives == 0) {
                game.gameOverSound.loop();
                game.setScreen(new MainMenuScreen(game));
            } else {
                game.downSound.loop();
                game.setScreen(new GameScreen(game));
            }
        }

        // Controla el límite izquierdo de la pantalla
        if (player.position.x <= 0)
            player.position.x = 0;

//        timeBetweenAttacks += stateTime;
//        kenCurrentFrame = kenIdle.get(kenDirection).getKeyFrame(stateTime);
//
//        //movement
//        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
//            kenPos.x -= 250 * Gdx.graphics.getDeltaTime();
//            kenCurrentFrame = kenWalk.get(Directions.LEFT).getKeyFrame(stateTime);
//            kenDirection = Directions.LEFT;
//        }
//        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
//            kenPos.x += 250 * Gdx.graphics.getDeltaTime();
//            kenCurrentFrame = kenWalk.get(Directions.RIGHT).getKeyFrame(stateTime);
//            kenDirection = Directions.RIGHT;
//        }
//
//
//        if (Gdx.input.isKeyPressed(Input.Keys.SPACE) && Gdx.input.isKeyPressed(Input.Keys.DOWN)  && timeBetweenAttacks >= ATTACK_RATE) {
//            kenCurrentFrame = kenDash.get(kenDirection).getKeyFrame(stateTime);
//            if (kenDirection == Directions.LEFT) {
//                kenPos.x -= 250 * Gdx.graphics.getDeltaTime();
//            } else {
//                kenPos.x += 250 * Gdx.graphics.getDeltaTime();
//            }
//            timeBetweenAttacks = 0;
//        } else if(Gdx.input.isKeyPressed(Input.Keys.SPACE)  && timeBetweenAttacks >= ATTACK_RATE) {
//            kenPos.y += 250 * Gdx.graphics.getDeltaTime();
//            kenCurrentFrame = kenJump.get(kenDirection).getKeyFrame(stateTime);
//            Timer.schedule(new Timer.Task() {
//                @Override
//                public void run() {
//                    kenPos.y = 180;
//                }
//            }, 0.7f);
//            timeBetweenAttacks = 0;
//        }
//
//        if (Gdx.input.isKeyPressed(Input.Keys.E)  && timeBetweenAttacks >= ATTACK_RATE) {
//            kenCurrentFrame = kenBasicAttack.get(kenDirection).getKeyFrame(stateTime);
//            timeBetweenAttacks = 0;
//        }
//
//        if (Gdx.input.isKeyPressed(Input.Keys.Q)  && timeBetweenAttacks >= ATTACK_RATE) {
//            kenCurrentFrame = kenGuard.get(kenDirection).getKeyFrame(stateTime);
//            timeBetweenAttacks = 0;
//        }
//
//
//        if (Gdx.input.isKeyPressed(Input.Keys.E) && Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
//            kenCurrentFrame = kenBasicAttackMove.get(Directions.RIGHT).getKeyFrame(stateTime);
//            timeBetweenAttacks = 0;
//        }
//
//        if (Gdx.input.isKeyPressed(Input.Keys.E) && Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
//            kenCurrentFrame = kenBasicAttackMove.get(Directions.LEFT).getKeyFrame(stateTime);
//            timeBetweenAttacks = 0;
//        }
//
//        if (Gdx.input.isKeyPressed(Input.Keys.E) && Gdx.input.isKeyPressed(Input.Keys.UP)) {
//            System.out.println("perfect");
//            kenCurrentFrame = kenBasicAttackUp.get(kenDirection).getKeyFrame(stateTime);
//            if (kenDirection == Directions.LEFT) {
//                kenPos.x -= 150 * Gdx.graphics.getDeltaTime();
//            } else {
//                kenPos.x += 150 * Gdx.graphics.getDeltaTime();
//            }
//            timeBetweenAttacks = 0;
//        }
//
//        if (Gdx.input.isKeyPressed(Input.Keys.E) && Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
//            System.out.println("perfect");
//            kenCurrentFrame = kenBasicAttackDown.get(kenDirection).getKeyFrame(stateTime);
//            if (kenDirection == Directions.LEFT) {
//                kenPos.x -= 150 * Gdx.graphics.getDeltaTime();
//            } else {
//                kenPos.x += 150 * Gdx.graphics.getDeltaTime();
//            }
//            timeBetweenAttacks = 0;
//        }
    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
