kenshin.png
format: RGBA8888
filter: Linear,Linear
repeat: none
kenshinbasicattackdownleft
  rotate: false
  xy: 42, 378
  size: 32, 51
  orig: 110, 110
  offset: 38, 34
  index: 1
kenshinbasicattackdownleft
  rotate: false
  xy: 275, 412
  size: 28, 51
  orig: 110, 110
  offset: 40, 35
  index: 2
kenshinbasicattackdownleft
  rotate: false
  xy: 374, 281
  size: 40, 51
  orig: 110, 110
  offset: 34, 34
  index: 3
kenshinbasicattackdownleft
  rotate: false
  xy: 202, 2
  size: 78, 48
  orig: 110, 110
  offset: 14, 34
  index: 4
kenshinbasicattackdownleft
  rotate: false
  xy: 202, 52
  size: 64, 46
  orig: 110, 110
  offset: 24, 33
  index: 5
kenshinbasicattackdownleft
  rotate: false
  xy: 231, 377
  size: 36, 49
  orig: 110, 110
  offset: 36, 36
  index: 6
kenshinbasicattackdownleft
  rotate: false
  xy: 305, 448
  size: 27, 52
  orig: 110, 110
  offset: 39, 35
  index: 7
kenshinbasicattackdownleft
  rotate: false
  xy: 476, 2
  size: 34, 53
  orig: 110, 110
  offset: 38, 34
  index: 8
kenshinbasicattackdownright
  rotate: false
  xy: 116, 356
  size: 32, 51
  orig: 110, 110
  offset: 42, 31
  index: 1
kenshinbasicattackdownright
  rotate: false
  xy: 188, 381
  size: 28, 51
  orig: 110, 110
  offset: 40, 34
  index: 2
kenshinbasicattackdownright
  rotate: false
  xy: 460, 294
  size: 40, 51
  orig: 110, 110
  offset: 34, 34
  index: 3
kenshinbasicattackdownright
  rotate: false
  xy: 2, 50
  size: 78, 48
  orig: 110, 110
  offset: 16, 35
  index: 4
kenshinbasicattackdownright
  rotate: false
  xy: 348, 2
  size: 64, 46
  orig: 110, 110
  offset: 23, 34
  index: 5
kenshinbasicattackdownright
  rotate: false
  xy: 150, 375
  size: 36, 49
  orig: 110, 110
  offset: 36, 35
  index: 6
kenshinbasicattackdownright
  rotate: false
  xy: 334, 448
  size: 27, 52
  orig: 110, 110
  offset: 41, 33
  index: 7
kenshinbasicattackdownright
  rotate: false
  xy: 476, 57
  size: 34, 53
  orig: 110, 110
  offset: 37, 32
  index: 8
kenshinbasicattackleft
  rotate: false
  xy: 152, 170
  size: 39, 52
  orig: 70, 80
  offset: 16, 14
  index: 1
kenshinbasicattackleft
  rotate: false
  xy: 313, 53
  size: 33, 51
  orig: 70, 80
  offset: 19, 16
  index: 2
kenshinbasicattackleft
  rotate: false
  xy: 2, 100
  size: 64, 49
  orig: 70, 80
  offset: 2, 16
  index: 3
kenshinbasicattackleft
  rotate: false
  xy: 245, 117
  size: 41, 49
  orig: 70, 80
  offset: 14, 14
  index: 4
kenshinbasicattackleft
  rotate: false
  xy: 42, 324
  size: 32, 52
  orig: 70, 80
  offset: 21, 16
  index: 5
kenshinbasicattackleftmove
  rotate: false
  xy: 234, 219
  size: 39, 48
  orig: 110, 70
  offset: 37, 11
  index: 1
kenshinbasicattackleftmove
  rotate: false
  xy: 2, 2
  size: 98, 46
  orig: 110, 70
  offset: 6, 10
  index: 2
kenshinbasicattackleftmove
  rotate: false
  xy: 82, 50
  size: 78, 46
  orig: 110, 70
  offset: 17, 10
  index: 3
kenshinbasicattackleftmove
  rotate: false
  xy: 80, 192
  size: 39, 46
  orig: 110, 70
  offset: 35, 9
  index: 4
kenshinbasicattackleftmove
  rotate: false
  xy: 288, 159
  size: 41, 49
  orig: 110, 70
  offset: 36, 10
  index: 5
kenshinbasicattackleftmove
  rotate: false
  xy: 152, 272
  size: 38, 52
  orig: 110, 70
  offset: 36, 11
  index: 6
kenshinbasicattackleftmove
  rotate: false
  xy: 41, 269
  size: 34, 53
  orig: 110, 70
  offset: 40, 11
  index: 7
kenshinbasicattackright
  rotate: false
  xy: 193, 177
  size: 39, 52
  orig: 70, 80
  offset: 17, 16
  index: 1
kenshinbasicattackright
  rotate: false
  xy: 313, 106
  size: 33, 51
  orig: 70, 80
  offset: 20, 17
  index: 2
kenshinbasicattackright
  rotate: false
  xy: 282, 2
  size: 64, 49
  orig: 70, 80
  offset: 5, 17
  index: 3
kenshinbasicattackright
  rotate: false
  xy: 245, 168
  size: 41, 49
  orig: 70, 80
  offset: 14, 15
  index: 4
kenshinbasicattackright
  rotate: false
  xy: 117, 302
  size: 32, 52
  orig: 70, 80
  offset: 16, 15
  index: 5
kenshinbasicattackrightmove
  rotate: false
  xy: 275, 258
  size: 39, 48
  orig: 70, 80
  offset: 17, 17
  index: 1
kenshinbasicattackrightmove
  rotate: false
  xy: 102, 2
  size: 98, 46
  orig: 110, 70
  offset: 6, 11
  index: 2
kenshinbasicattackrightmove
  rotate: false
  xy: 82, 98
  size: 78, 46
  orig: 110, 70
  offset: 15, 9
  index: 3
kenshinbasicattackrightmove
  rotate: false
  xy: 80, 240
  size: 39, 46
  orig: 110, 70
  offset: 35, 12
  index: 4
kenshinbasicattackrightmove
  rotate: false
  xy: 331, 196
  size: 41, 49
  orig: 110, 70
  offset: 34, 10
  index: 5
kenshinbasicattackrightmove
  rotate: false
  xy: 234, 269
  size: 38, 52
  orig: 110, 70
  offset: 36, 11
  index: 6
kenshinbasicattackrightmove
  rotate: false
  xy: 74, 435
  size: 34, 53
  orig: 110, 70
  offset: 36, 10
  index: 7
kenshinbasicattackupleft
  rotate: false
  xy: 316, 294
  size: 39, 48
  orig: 110, 110
  offset: 38, 34
  index: 1
kenshinbasicattackupleft
  rotate: false
  xy: 288, 210
  size: 41, 46
  orig: 110, 110
  offset: 34, 32
  index: 2
kenshinbasicattackupleft
  rotate: false
  xy: 414, 2
  size: 60, 80
  orig: 110, 110
  offset: 23, 19
  index: 3
kenshinbasicattackupleft
  rotate: false
  xy: 393, 156
  size: 41, 75
  orig: 110, 110
  offset: 35, 22
  index: 4
kenshinbasicattackupleft
  rotate: false
  xy: 268, 53
  size: 43, 62
  orig: 110, 110
  offset: 35, 24
  index: 5
kenshinbasicattackupleft
  rotate: false
  xy: 231, 377
  size: 36, 49
  orig: 110, 110
  offset: 36, 32
  index: 6
kenshinbasicattackupleft
  rotate: false
  xy: 305, 448
  size: 27, 52
  orig: 110, 110
  offset: 42, 33
  index: 7
kenshinbasicattackupleft
  rotate: false
  xy: 476, 2
  size: 34, 53
  orig: 110, 110
  offset: 37, 33
  index: 8
kenshinbasicattackupright
  rotate: false
  xy: 357, 334
  size: 39, 48
  orig: 110, 70
  offset: 34, 12
  index: 1
kenshinbasicattackupright
  rotate: false
  xy: 436, 184
  size: 41, 46
  orig: 110, 70
  offset: 34, 12
  index: 2
kenshinbasicattackupright
  rotate: false
  xy: 348, 50
  size: 60, 80
  orig: 110, 110
  offset: 25, 18
  index: 3
kenshinbasicattackupright
  rotate: false
  xy: 202, 100
  size: 41, 75
  orig: 110, 110
  offset: 33, 16
  index: 4
kenshinbasicattackupright
  rotate: false
  xy: 348, 132
  size: 43, 62
  orig: 110, 110
  offset: 33, 25
  index: 5
kenshinbasicattackupright
  rotate: false
  xy: 150, 375
  size: 36, 49
  orig: 110, 110
  offset: 35, 28
  index: 6
kenshinbasicattackupright
  rotate: false
  xy: 334, 448
  size: 27, 52
  orig: 110, 110
  offset: 41, 29
  index: 7
kenshinbasicattackupright
  rotate: false
  xy: 476, 57
  size: 34, 53
  orig: 110, 110
  offset: 37, 30
  index: 8
kenshindashleft
  rotate: false
  xy: 410, 84
  size: 49, 34
  orig: 70, 80
  offset: 12, 22
  index: 1
kenshindashleft
  rotate: false
  xy: 410, 120
  size: 47, 34
  orig: 70, 80
  offset: 13, 23
  index: 2
kenshindashright
  rotate: false
  xy: 461, 112
  size: 49, 34
  orig: 70, 80
  offset: 9, 23
  index: 1
kenshindashright
  rotate: false
  xy: 459, 148
  size: 47, 34
  orig: 70, 80
  offset: 10, 23
  index: 2
kenshinguardleft
  rotate: false
  xy: 2, 422
  size: 37, 57
  orig: 70, 80
  offset: 19, 12
  index: 1
kenshinguardleft
  rotate: false
  xy: 162, 50
  size: 38, 58
  orig: 70, 80
  offset: 16, 11
  index: 2
kenshinguardleft
  rotate: false
  xy: 149, 448
  size: 30, 53
  orig: 70, 80
  offset: 22, 13
  index: 3
kenshinguardright
  rotate: false
  xy: 2, 269
  size: 37, 57
  orig: 70, 80
  offset: 15, 12
  index: 1
kenshinguardright
  rotate: false
  xy: 162, 110
  size: 38, 58
  orig: 70, 80
  offset: 14, 12
  index: 2
kenshinguardright
  rotate: false
  xy: 181, 448
  size: 30, 53
  orig: 70, 80
  offset: 18, 13
  index: 3
kenshinidleleft
  rotate: false
  xy: 121, 192
  size: 29, 53
  orig: 50, 65
  offset: 9, 6
  index: 1
kenshinidleleft
  rotate: false
  xy: 121, 247
  size: 29, 53
  orig: 50, 65
  offset: 10, 6
  index: 2
kenshinidleleft
  rotate: false
  xy: 479, 184
  size: 31, 53
  orig: 50, 65
  offset: 8, 6
  index: 3
kenshinidleleft
  rotate: false
  xy: 479, 239
  size: 31, 53
  orig: 50, 65
  offset: 9, 7
  index: 4
kenshinidleright
  rotate: false
  xy: 213, 448
  size: 29, 53
  orig: 50, 65
  offset: 12, 6
  index: 1
kenshinidleright
  rotate: false
  xy: 244, 448
  size: 29, 53
  orig: 50, 65
  offset: 11, 6
  index: 2
kenshinidleright
  rotate: false
  xy: 41, 431
  size: 31, 53
  orig: 50, 65
  offset: 11, 6
  index: 3
kenshinidleright
  rotate: false
  xy: 116, 409
  size: 31, 53
  orig: 50, 65
  offset: 10, 7
  index: 4
kenshinjumpleft
  rotate: false
  xy: 2, 224
  size: 37, 41
  orig: 70, 80
  offset: 14, 21
  index: 0
kenshinjumpleft
  rotate: false
  xy: 274, 308
  size: 38, 52
  orig: 70, 80
  offset: 19, 15
  index: 1
kenshinjumpleft
  rotate: false
  xy: 192, 279
  size: 38, 52
  orig: 70, 80
  offset: 13, 14
  index: 2
kenshinjumpleft
  rotate: false
  xy: 416, 327
  size: 40, 44
  orig: 70, 80
  offset: 18, 15
  index: 3
kenshinjumpleft
  rotate: false
  xy: 458, 347
  size: 40, 44
  orig: 70, 80
  offset: 12, 18
  index: 4
kenshinjumpleft
  rotate: false
  xy: 2, 224
  size: 37, 41
  orig: 70, 80
  offset: 21, 17
  index: 5
kenshinjumpleft
  rotate: false
  xy: 2, 384
  size: 38, 36
  orig: 70, 80
  offset: 17, 22
  index: 6
kenshinjumpleft
  rotate: false
  xy: 2, 224
  size: 37, 41
  orig: 70, 80
  offset: 17, 23
  index: 7
kenshinjumpright
  rotate: false
  xy: 41, 224
  size: 37, 41
  orig: 70, 80
  offset: 18, 20
  index: 0
kenshinjumpright
  rotate: false
  xy: 232, 323
  size: 38, 52
  orig: 70, 80
  offset: 19, 14
  index: 1
kenshinjumpright
  rotate: false
  xy: 314, 344
  size: 38, 52
  orig: 70, 80
  offset: 19, 21
  index: 2
kenshinjumpright
  rotate: false
  xy: 68, 146
  size: 40, 44
  orig: 70, 80
  offset: 17, 21
  index: 3
kenshinjumpright
  rotate: false
  xy: 110, 146
  size: 40, 44
  orig: 70, 80
  offset: 18, 16
  index: 4
kenshinjumpright
  rotate: false
  xy: 41, 224
  size: 37, 41
  orig: 70, 80
  offset: 18, 18
  index: 5
kenshinjumpright
  rotate: false
  xy: 2, 344
  size: 38, 36
  orig: 70, 80
  offset: 17, 22
  index: 6
kenshinjumpright
  rotate: false
  xy: 41, 224
  size: 37, 41
  orig: 70, 80
  offset: 17, 19
  index: 7
kenshinrunleft
  rotate: false
  xy: 436, 232
  size: 41, 46
  orig: 50, 65
  offset: 4, 4
  index: 0
kenshinrunleft
  rotate: false
  xy: 76, 338
  size: 38, 47
  orig: 50, 65
  offset: 7, 7
  index: 1
kenshinrunleft
  rotate: false
  xy: 272, 362
  size: 38, 48
  orig: 50, 65
  offset: 4, 6
  index: 2
kenshinrunleft
  rotate: false
  xy: 398, 373
  size: 39, 47
  orig: 50, 65
  offset: 7, 6
  index: 3
kenshinrunleft
  rotate: false
  xy: 331, 247
  size: 41, 45
  orig: 50, 65
  offset: 3, 4
  index: 4
kenshinrunleft
  rotate: false
  xy: 76, 387
  size: 38, 46
  orig: 50, 65
  offset: 7, 4
  index: 5
kenshinrunleft
  rotate: false
  xy: 354, 384
  size: 38, 48
  orig: 50, 65
  offset: 5, 5
  index: 6
kenshinrunleft
  rotate: false
  xy: 152, 224
  size: 39, 46
  orig: 50, 65
  offset: 7, 6
  index: 7
kenshinrunright
  rotate: false
  xy: 374, 233
  size: 41, 46
  orig: 50, 65
  offset: 5, 7
  index: 0
kenshinrunright
  rotate: false
  xy: 151, 326
  size: 38, 47
  orig: 50, 65
  offset: 6, 7
  index: 1
kenshinrunright
  rotate: false
  xy: 312, 398
  size: 38, 48
  orig: 50, 65
  offset: 6, 7
  index: 2
kenshinrunright
  rotate: false
  xy: 2, 151
  size: 39, 47
  orig: 50, 65
  offset: 6, 7
  index: 3
kenshinrunright
  rotate: false
  xy: 417, 280
  size: 41, 45
  orig: 50, 65
  offset: 6, 6
  index: 4
kenshinrunright
  rotate: false
  xy: 191, 333
  size: 38, 46
  orig: 50, 65
  offset: 8, 5
  index: 5
kenshinrunright
  rotate: false
  xy: 77, 288
  size: 38, 48
  orig: 50, 65
  offset: 6, 6
  index: 6
kenshinrunright
  rotate: false
  xy: 193, 231
  size: 39, 46
  orig: 50, 65
  offset: 5, 6
  index: 7
