package cl.navicrew.game;


import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * Created by liot on 10/13/14.
 */
public class KenshinAdventureDesktop {
    public static void main(String[] args) {
        LwjglApplicationConfiguration configuration = new LwjglApplicationConfiguration();
        configuration.title = "KenshinAdventure!";
        configuration.resizable = false;
        configuration.fullscreen = false;

        new LwjglApplication(new KenshinAdventure(), configuration);
    }
}
