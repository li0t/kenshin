package cl.navicrew.game;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import superJumper.Jumper2DX;

/**
 * Created by liot on 10/18/14.
 */
public class SuperJumperDesktop {
    public static void main(String[] args) {
        LwjglApplicationConfiguration configuration = new LwjglApplicationConfiguration();
        configuration.title = "KenshinAdventure!";
        configuration.resizable = false;
        configuration.fullscreen = false;

        new LwjglApplication(new Jumper2DX(), configuration);
    }
}
